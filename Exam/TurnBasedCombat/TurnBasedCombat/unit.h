
class unit
{
public:
	unit();
	~unit();
};
#pragma once

#include "Skill.h"
#include <vector>
#include <string>

//class HealingItem;
class Skill;

class Unit
{
public:
	Unit(std::string name);
	~Unit();

	// Basic
	std::string getName();
	int getHp();
	int getHpMax();
	int getMp();
	int getMpMax();
	int getVitality();
	int getPower();
	//void addExp(int exp);
	int getDexterity();

	void addHp(int amount);
	void takeDamage(int damage);
	void heal(int amount);
	
	// Items
	

	// Skills
	void useSkill(Skill* skill);

	
	

private:
	// Basic
	std::string mName;
	int mLevel;
	int mHp;
	int mHpMax;
	int mMp;
	int mMpMax;
	int mPower;
	int mVitality;
	int mDexterity;

	// Equipment
	
	// Items


	// Skills
	std::vector<Skill*> mSkills;

	

};

