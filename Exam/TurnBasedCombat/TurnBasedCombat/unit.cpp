#include "Unit.h"
#include <iostream>

using namespace std;

Unit::Unit(std::string name)
{
	mName = name;

	// Start with 1000 HP
	mHp = 1000;
	mHpMax = 1000;
}

Unit::~Unit()
{
}

std::string Unit::getName()
{
	return mName;
}


int Unit::getHp()
{
	return mHp;
}

int Unit::getHpMax()
{
	return mHpMax;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getMpMax()
{
	return mMpMax;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getPower()
{
	return mPower;
}
int Unit::getDexterity()
{
	return mDexterity;
}
void Unit::addHp(int amount)
{
	mHp += amount;
	if (mHp > mHpMax) mHp = mHpMax;
}

void Unit::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Unit::heal(int amount)
{
	mHp += amount;
	if (mHp > mHpMax) mHp = mHpMax;
}
