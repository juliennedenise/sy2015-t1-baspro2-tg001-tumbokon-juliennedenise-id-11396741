#include <iostream>
#include <string>
using namespace std;

class Shape
{
public:

	Shape(string type, float area, float perimeter)
		: mType(type), mArea(area), mPerimeter(perimeter);
	
	
protected:

	string mType;
	float mArea;
	float mPerimeter;

	Rectangle* mRectangle;
	Square* mSquare;
	Circle* mCircle;

};

	class Rectangle : public Shape
	{

	public:
		Rectangle(float l, float w);
		
		float area = l * w;
		float perimeter = 2 * (l + w)


	private:
		int mL;
		int mW;
		float area = mL * mW;
		float perimeter = 2 * (mL + mW)

	};

	class Circle : public Shape
	{

	public:

			Circle(float r);
    private:
		int mR;
		
		float area = 3.14 * (mR*mR) ;
		float perimeter = 2 * 3.14 *mR;

	};

	class Square : public Shape
	{

	public:

			Square(float s);
    private:

		int mS;

	};

	

void main()
{
	Shape* shape[3];
	shape[0] = new Rectangle(1, 2);
	shape[1] = new Circle(4);
	shape[2] = new Square(4);



}

	
